package com.allier.domain

/**
 * Representation of a floor level
 */
data class Height(val value: Double) {
    operator fun minus(height: Height): Height = Height(this.value - height.value)
}

operator fun Height.div(d: Double): Double = this.value / d