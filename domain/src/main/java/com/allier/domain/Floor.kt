package com.allier.domain

/**
 * Representation of a floor level
 */
data class Floor(val level: Int)