package com.allier.domain

import org.amshove.kluent.shouldEqual
import org.junit.Test

class HeightTest {

    @Test
    fun shouldMinusProperly() {
        Height(4.0) - Height(1.0) shouldEqual Height(3.0)
    }

    @Test
    fun shouldDivProperly() {
        Height(5.0) / 2.0 shouldEqual 2.5
    }

}