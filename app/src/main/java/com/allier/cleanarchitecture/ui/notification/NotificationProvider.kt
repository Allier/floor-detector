package com.allier.cleanarchitecture.ui.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.allier.cleanarchitecture.R
import com.allier.cleanarchitecture.framework.BarometerService
import com.allier.cleanarchitecture.framework.COMMAND_STOP
import com.allier.cleanarchitecture.framework.KEY_COMMAND
import com.allier.cleanarchitecture.ui.main.MainActivity
import com.allier.domain.Floor
import com.allier.usecases.GetCurrentFloorUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

const val CHANNEL_ID = "floor-detector-channel"
const val ONGOING_NOTIFICATION_ID = 0x0f
const val REQUEST_CODE_ACTIVITY = 0x088
const val REQUEST_CODE_SERVICE = 0x089

/**
 * Provides access to application notification
 * and updates it's data on floor changes
 */
class NotificationProvider(private val context: Context,
                           private val getCurrentFloorUseCase: GetCurrentFloorUseCase) {

    private val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    private val notificationBuilder: NotificationCompat.Builder

    init {
        createChannel()

        val activityPendingIntent = getMainActivityPendingIntent()
        val stopServicePendingIntent = getStopServicePendingIntent()
        val stopAction = getStopServiceAction(stopServicePendingIntent)

        notificationBuilder = prepareNotificationBuilder(activityPendingIntent, stopAction)
    }

    private fun prepareNotificationBuilder(activityPendingIntent: PendingIntent,
                                           stopAction: NotificationCompat.Action)
            = NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(context.getString(R.string.notification_name))
                .setContentText(getContentText())
                .setContentIntent(activityPendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setStyle(NotificationCompat.BigTextStyle())
                .addAction(stopAction)

    /**
     * Builds predefined notification
     */
    fun provideNotification(): Notification = notificationBuilder.build()


    private var floorChangesDisposable: Disposable? = null

    /**
     * Start responding to floor changes with notification text updates and sound
     */
    fun observeFloorChanges() {
        disposeFloorChangesObserving()

        floorChangesDisposable = getCurrentFloorUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .skip(1)
                .subscribe(this::updateNotificationFloor)
    }

    /**
     * Stop responding to floor changes events
     */
    fun disposeFloorChangesObserving() {
        if (floorChangesDisposable?.isDisposed == false) floorChangesDisposable?.dispose()
    }

    private fun updateNotificationFloor(it: Floor) {
        notificationBuilder.setContentText(getContentText(it.level))
        notificationManager.notify(ONGOING_NOTIFICATION_ID, provideNotification())
    }


    private fun createChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channelName = "Barometer channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val notificationChannel = NotificationChannel(CHANNEL_ID, channelName, importance)
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun getMainActivityPendingIntent() = PendingIntent.getActivity(
            context,
            REQUEST_CODE_ACTIVITY,
            Intent(context.applicationContext, MainActivity::class.java).apply {
                action = Intent.ACTION_VIEW
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
            },
            FLAG_UPDATE_CURRENT)

    private fun getStopServicePendingIntent() = PendingIntent.getService(
            context,
            REQUEST_CODE_SERVICE,
            Intent(context, BarometerService::class.java).apply { putExtra(KEY_COMMAND, COMMAND_STOP) },
            FLAG_UPDATE_CURRENT)

    private fun getStopServiceAction(stopServicePendingIntent: PendingIntent) =
            NotificationCompat.Action.Builder(
                    0,
                    context.getString(R.string.power_button_stop),
                    stopServicePendingIntent)
                    .build()

    private fun getContentText(floor: Int = getCurrentFloorUseCase.getCurrentLevelOnly()) =
            context.getString(R.string.notification_text, floor)

}


