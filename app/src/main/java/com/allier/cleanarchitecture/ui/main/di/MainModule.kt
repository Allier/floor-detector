package com.allier.cleanarchitecture.ui.main.di

import com.allier.cleanarchitecture.App
import com.allier.usecases.GetCurrentFloorUseCase
import dagger.Module
import dagger.Provides

/**
 * MainActivity-specific dependencies
 */
@Module
class MainModule {

    @Provides
    internal fun provideMainViewModelFactory(app: App, getCurrentFloorUseCase: GetCurrentFloorUseCase)
            = MainViewModelFactory(app, getCurrentFloorUseCase)

}
