package com.allier.cleanarchitecture.ui.main

import android.app.Application
import android.content.Intent
import android.os.Build
import androidx.core.content.ContextCompat.startForegroundService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.allier.cleanarchitecture.App
import com.allier.cleanarchitecture.framework.BarometerService
import com.allier.domain.Floor
import com.allier.usecases.GetCurrentFloorUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Part of MVVM pattern implementation - ViewModel of MainActivity
 * Binds changes between Data (FloorRepository) and UI with help of GetCurrentFloorUseCase
 */
class MainViewModel(app: App, private val getCurrentFloorUseCase: GetCurrentFloorUseCase) : AndroidViewModel(app) {

    internal val currentFloor = MutableLiveData<Floor>()

    private val disposables = CompositeDisposable()

    /**
     * Starts tracking floor changes events
     * using BarometerService to keep tracking in background
     */
    fun startTracking() {
        startBarometerService()
        observeFloorChanges()
    }

    /**
     * Stops tracking floor changes events
     * Also stops BarometerService
     */
    fun stopTracking() {
        stopFloorChangesObserving()
        stopBarometerService()
    }

    override fun onCleared() {
        stopFloorChangesObserving()
    }


    private fun startBarometerService() {
        val intent = Intent(getApplication(), BarometerService::class.java)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            getApplication<Application>().startService(intent)
        } else {
            startForegroundService(getApplication(), intent)
        }
    }

    private fun stopBarometerService() {
        getApplication<Application>().stopService(Intent(getApplication(), BarometerService::class.java))
    }


    private fun observeFloorChanges() {
        disposables.add(getCurrentFloorUseCase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { currentFloor.value = it })
    }

    private fun stopFloorChangesObserving() {
        disposables.clear()
    }

}
