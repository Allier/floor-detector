package com.allier.cleanarchitecture.ui.main.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.allier.cleanarchitecture.App
import com.allier.cleanarchitecture.ui.main.MainViewModel
import com.allier.usecases.GetCurrentFloorUseCase

/**
 * Factory pattern implementation for MainViewModel creation
 */
class MainViewModelFactory internal constructor(
        private val app: App,
        private val getCurrentFloorUseCase: GetCurrentFloorUseCase)
    : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(app, getCurrentFloorUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
