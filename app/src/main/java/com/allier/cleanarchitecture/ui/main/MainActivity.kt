package com.allier.cleanarchitecture.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.allier.cleanarchitecture.R
import com.allier.cleanarchitecture.framework.*
import com.allier.cleanarchitecture.ui.FloorHeightDialog
import com.allier.cleanarchitecture.ui.main.di.MainViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * Represents UI state of MainActivity
 */
private enum class State {
    /**
     * Idle UI state (Start button should be shown)
     */
    IDLE,

    /**
     * Running UI state (Stop button should be shown)
     */
    RUNNING
}

/**
 * View representation of MainActivity
 * Responsible of binding user interactions and ViewModel
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject lateinit var viewModelFactory: MainViewModelFactory

    private lateinit var viewModel: MainViewModel
    private lateinit var screenAnimator: MainScreenAnimator
    private lateinit var state: State

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        state = State.IDLE
        screenAnimator = MainScreenAnimator(startBtn, stopBtn, floor, floorLbl, notice)

        setupStartButton()
        setupStopButton()

        viewModel.currentFloor.observe(this, Observer { floor.text = it.level.toString() })

        pingBarometerService()
        startBtn.post(this::displayState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.actionFloorHeight -> {
            FloorHeightDialog().show(supportFragmentManager, "edit_floor_height")
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    private fun setupStartButton() = startBtn.setOnClickListener { start() }

    private fun setupStopButton() = stopBtn.setOnClickListener { stop() }

    private fun start() {
        screenAnimator.startToStopAnimation()
        viewModel.startTracking()
    }

    private fun stop() {
        screenAnimator.stopToStartAnimation()
        viewModel.stopTracking()
    }

    private fun displayState() {
        when (state) {
            State.IDLE -> {
                startBtn.show()
                notice.show()
            }
            State.RUNNING -> start()
        }
    }


    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(KEY_COMMAND, 0)) {
                COMMAND_STOP -> stop()
                COMMAND_PONG -> state = State.RUNNING
            }
        }
    }

    private fun pingBarometerService() {
        castPressureCommand(this, COMMAND_PING)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, PRESSURE_INTENT_FILTER)
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        super.onPause()
    }
}
