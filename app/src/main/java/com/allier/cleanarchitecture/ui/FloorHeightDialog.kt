package com.allier.cleanarchitecture.ui

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.allier.cleanarchitecture.R
import com.allier.usecases.GetFloorHeightUseCase
import com.allier.usecases.SetFloorHeightUseCase
import com.jakewharton.rxbinding3.widget.textChanges
import dagger.android.support.DaggerAppCompatDialogFragment
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Dialog that is used to change floor height parameter
 */
class FloorHeightDialog : DaggerAppCompatDialogFragment() {

    private val disposables = CompositeDisposable()

    @Inject lateinit var setFloorHeightUseCase: SetFloorHeightUseCase
    @Inject lateinit var getFloorHeightUseCase: GetFloorHeightUseCase
    private lateinit var heightEditText: EditText

    private val handlePositiveClick = DialogInterface.OnClickListener { _, _ ->
        setFloorHeightUseCase(heightEditText.text.toString().toDouble())
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        if (activity == null) return super.onCreateDialog(savedInstanceState)
        prepareDialogView()
        return buildDialog()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showSoftKeyboard()
    }

    override fun onDismiss(dialog: DialogInterface) {
        disposables.clear()
        super.onDismiss(dialog)
    }


    private fun prepareDialogView() {
        heightEditText = EditText(activity).apply {
            inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
            setHint(R.string.floor_height_hint)
            setText(getFloorHeightUseCase().toString())
        }
    }

    private fun buildDialog(): AlertDialog {
        val heightDialog = AlertDialog.Builder(activity!!)
                .setTitle(R.string.floor_height_title)
                .setView(heightEditText)
                .setPositiveButton(android.R.string.ok, handlePositiveClick)
                .setNegativeButton(android.R.string.cancel, null)
                .create()

        heightDialog.setOnShowListener { dialog ->
            disposables.add(heightEditText.textChanges()
                    .map { charSequence -> charSequence.isNotEmpty() }
                    .subscribe { enabled -> (dialog as AlertDialog).getButton(DialogInterface.BUTTON_POSITIVE).isEnabled = enabled })
        }

        return heightDialog
    }

    private fun showSoftKeyboard() {
        heightEditText.requestFocus()
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

}