package com.allier.cleanarchitecture.ui.main

import android.view.View
import android.view.ViewPropertyAnimator

private const val defaultDuration = 400L

/**
 * Simplified access to view animation with default duration
 * @see defaultDuration
 */
fun View.morph(): ViewPropertyAnimator = this.animate().setDuration(defaultDuration)

/**
 * Change alpha state of the view to fully visible (1) with animation
 * @see morph
 */
fun View.show(): ViewPropertyAnimator = this.morph().alpha(1f)

/**
 * Change alpha state of the view to fully invisible (1) with animation
 * @see morph
 */
fun View.hide(): ViewPropertyAnimator = this.morph().alpha(0f)

/**
 * Helper class that is tightly related to exact MainActivity UI implementation
 * and responsible for animations between idle and floor detection running states
 */
class MainScreenAnimator(private val startBtn: View,
                         private val stopBtn: View,
                         private val floor: View,
                         private val floorLbl: View,
                         private val notice: View) {

    private var lastY = 0f

    /**
     * Animate transaction from idle to running UI state
     */
    fun startToStopAnimation() {
        lastY = startBtn.y

        notice.hide()
        startBtnAnimation(stopBtn.y * 0.9f, 0f).withEndAction {
            stopBtnAnimation(1f).withEndAction { showFloor() }
        }
    }

    /**
     * Animate transaction from running to idle UI state
     */
    fun stopToStartAnimation() {
        stopBtnAnimation(0f).withEndAction {
            hideFloor()
            startBtnAnimation(lastY, 1f).withEndAction { notice.show() }
        }
    }

    private fun startBtnAnimation(y: Float, value: Float) = startBtn.animate()
            .setDuration(defaultDuration + 200L)
            .y(y)
            .scaleX(value)
            .scaleY(value)
            .alpha(value)

    private fun stopBtnAnimation(value: Float) = stopBtn.morph().scaleX(value)

    private fun showFloor() {
        floor.show()
        floorLbl.show()
    }

    private fun hideFloor() {
        floor.hide()
        floorLbl.hide()
    }

}