package com.allier.cleanarchitecture.framework

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager

/**
 * Intent action name of MainActivity and BarometerService communications filter
 */
const val PRESSURE_INTENT_ACTION = "PRESSURE"

/**
 * IntentFilter of MainActivity and BarometerService communications
*/
val PRESSURE_INTENT_FILTER = IntentFilter(PRESSURE_INTENT_ACTION)

/**
 * Simplified access to broadcasting a message
 * MainActivity and BarometerService communications
 */
fun castPressureCommand(context: Context, command: Int) =
        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(Intent()
                .putExtra(KEY_COMMAND, command).apply { action = PRESSURE_INTENT_ACTION })