package com.allier.cleanarchitecture.framework.di

import com.allier.cleanarchitecture.App
import com.allier.cleanarchitecture.framework.BarometricHeightProvider
import com.allier.cleanarchitecture.ui.notification.NotificationProvider
import com.allier.data.FloorRepository
import com.allier.usecases.GetCurrentFloorUseCase
import com.allier.usecases.ResetFloorRepositoryUseCase
import com.allier.usecases.UpdateCurrentFloorUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Application-wide dependencies
 */
@Module
class AppModule {

    @Singleton
    @Provides
    internal fun provideFloorRepository() = FloorRepository()

    @Singleton
    @Provides
    internal fun provideHeightProvider(app: App,
                                       updateCurrentFloorUseCase: UpdateCurrentFloorUseCase,
                                       resetFloorRepositoryUseCase: ResetFloorRepositoryUseCase)
            = BarometricHeightProvider(app, updateCurrentFloorUseCase, resetFloorRepositoryUseCase)

    @Singleton
    @Provides
    internal fun provideNotificationProvider(context: App, getCurrentFloorUseCase: GetCurrentFloorUseCase)
            = NotificationProvider(context, getCurrentFloorUseCase)

}
