package com.allier.cleanarchitecture.framework.di

import com.allier.cleanarchitecture.framework.BarometerService
import com.allier.cleanarchitecture.ui.FloorHeightDialog
import com.allier.cleanarchitecture.ui.main.MainActivity
import com.allier.cleanarchitecture.ui.main.di.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Application-wide sub-components binding.
 */
@Module
abstract class BuildersModule {

    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBarometricService(): BarometerService

    @ContributesAndroidInjector
    internal abstract fun contributeFloorHeightDialog(): FloorHeightDialog

}
