package com.allier.cleanarchitecture.framework

import android.content.Context
import android.content.Context.SENSOR_SERVICE
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.allier.usecases.ResetFloorRepositoryUseCase
import com.allier.usecases.UpdateCurrentFloorUseCase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Altitude values provider
 * Altitude calculations are based on device Barometer sensor
 * Delivers altitude updates to FloorRepository via UpdateCurrentFloorUseCase
 */
class BarometricHeightProvider(context: Context,
                               private val updateCurrentFloorUseCase: UpdateCurrentFloorUseCase,
                               private val resetFloorRepositoryUseCase: ResetFloorRepositoryUseCase) {

    private val sensorManager: SensorManager
    private val pressureSensor: Sensor

    private var p0 = -1f

    private val pressureListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent) {
            if (p0 < 0) {
                p0 = event.values[0]
                return
            }

            GlobalScope.launch {
                val altitude = SensorManager.getAltitude(p0, event.values[0])
                updateCurrentFloorUseCase(altitude.toDouble())
            }
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
    }

    init {
        sensorManager = context.getSystemService(SENSOR_SERVICE) as SensorManager
        pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
    }

    /**
     * Start tracking altitude changes
     */
    fun activate() {
        sensorManager.registerListener(pressureListener, pressureSensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    /**
     * Stop tracking altitude changes
     */
    fun deactivate() {
        sensorManager.unregisterListener(pressureListener, pressureSensor)
        p0 = -1f
        resetFloorRepositoryUseCase()
    }

}