package com.allier.cleanarchitecture.framework

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.annotation.Nullable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.allier.cleanarchitecture.ui.notification.NotificationProvider
import com.allier.cleanarchitecture.ui.notification.ONGOING_NOTIFICATION_ID
import dagger.android.DaggerService
import javax.inject.Inject

/**
 * Intent parameter лун
 */
const val KEY_COMMAND = "service-command"

/**
 * Intent parameter value which is used as a stop service command
 */
const val COMMAND_STOP = 0x010

/**
 * Intent parameter value which is used as a ping from MainActivity to BarometerService
 */
const val COMMAND_PING = 0x0100

/**
 * Intent parameter value which is used by BarometerService as a response to COMMAND_PING
 */
const val COMMAND_PONG = 0x0200


/**
 * Foreground service responsible for keeping floor detection running
 */
class BarometerService : DaggerService() {

    @Inject lateinit var barometricHeightProvider: BarometricHeightProvider
    @Inject lateinit var notificationProvider: NotificationProvider

    private val echoReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.getIntExtra(KEY_COMMAND, 0)) {
                COMMAND_PING -> castPressureCommand(applicationContext, COMMAND_PONG)
            }
        }
    }


    @Nullable
    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when (intent.getIntExtra(KEY_COMMAND, 0)) {
            COMMAND_STOP -> {
                notifyStop()
                stopSelf()
            }
            else -> doStartService()
        }

        return START_NOT_STICKY
    }

    override fun onDestroy() {
        notificationProvider.disposeFloorChangesObserving()
        stopForeground(true)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(echoReceiver)
        barometricHeightProvider.deactivate()
        super.onDestroy()
    }


    private fun notifyStop() = castPressureCommand(applicationContext, COMMAND_STOP)

    private fun doStartService() {
        LocalBroadcastManager.getInstance(this).registerReceiver(echoReceiver, PRESSURE_INTENT_FILTER)
        barometricHeightProvider.activate()
        startForeground(ONGOING_NOTIFICATION_ID, notificationProvider.provideNotification())
        notificationProvider.observeFloorChanges()
    }

}
