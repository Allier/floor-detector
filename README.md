Floor Detector

The main goal of the app is to detect the floor-change activity of the user assuming he starts from the “ground” floor (which will be taken as a zero floor).

To do so a barometer equipped smartphone needs to be used to measure the change in barometric pressure from the starting point to the user’s current location.

This barometric pressure change is translated to the relative change in height first and then to an absolute floor level.

This tracking runs in a foreground Service, shows persistent notification to user and play a sound when floor level has changed.
