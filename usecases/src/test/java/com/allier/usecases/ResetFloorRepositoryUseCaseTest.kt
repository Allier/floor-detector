package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Floor
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test


class ResetFloorRepositoryUseCaseTest {

    lateinit var resetFloorRepositoryUseCase: ResetFloorRepositoryUseCase

    private var floorRepository = FloorRepository()

    @Before
    fun setUp() {
        resetFloorRepositoryUseCase = ResetFloorRepositoryUseCase(floorRepository)
    }

    @Test
    fun shouldResetRepository() {
        val lastFloor = FloorRepository::class.java.getDeclaredField("lastFloor").apply {
            isAccessible = true
        }
        lastFloor.set(floorRepository, Floor(3))

        resetFloorRepositoryUseCase()

        lastFloor.get(floorRepository) shouldEqual null
    }

}
