package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Height
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test

class SetFloorHeightUseCaseTest {

    lateinit var setFloorHeightUseCase: SetFloorHeightUseCase

    private val floorRepository = FloorRepository()

    @Before
    fun setUp() {
        setFloorHeightUseCase = SetFloorHeightUseCase(floorRepository)
    }

    @Test
    fun shouldSetFloorHeight() {
        setFloorHeightUseCase(10.0)
        floorRepository.floorHeight shouldEqual Height(10.0)
    }

}
