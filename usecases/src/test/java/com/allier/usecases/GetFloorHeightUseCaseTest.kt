package com.allier.usecases

import com.allier.data.FloorRepository
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test

class GetFloorHeightUseCaseTest {

    lateinit var getFloorHeightUseCase: GetFloorHeightUseCase

    var floorRepository = FloorRepository()

    @Before
    fun setUp() {
        getFloorHeightUseCase = GetFloorHeightUseCase(floorRepository)
    }

    @Test
    fun shouldGetFloorHeight() {
        getFloorHeightUseCase() shouldEqual 4.0
    }

}
