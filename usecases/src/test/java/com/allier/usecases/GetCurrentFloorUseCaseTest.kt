package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Floor
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test


class GetCurrentFloorUseCaseTest {

    lateinit var getCurrentFloorUseCase: GetCurrentFloorUseCase

    var testObserver = TestObserver<Floor>()

    @Before
    fun setUp() {
        val mock = mock<FloorRepository> {
            on { getCurrentFloor() } doReturn Observable.just(Floor(1), Floor(2))
            on { getCurrentFloorValue() } doReturn Floor(5)
        }
        getCurrentFloorUseCase = GetCurrentFloorUseCase(mock)
    }

    @Test
    fun shouldGetCurrentFloor() {
        getCurrentFloorUseCase().subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValueCount(2)
        testObserver.assertValues(Floor(1), Floor(2))
    }

    @Test
    fun shouldGetCurrentFloorValue() {
        getCurrentFloorUseCase.getCurrentLevelOnly() shouldEqual 5
    }

}
