package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Floor
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test


class UpdateCurrentFloorUseCaseTest {

    lateinit var updateCurrentFloorUseCase: UpdateCurrentFloorUseCase

    var floorRepository = FloorRepository()

    var testObserver = TestObserver<Floor>()

    @Before
    fun setUp() {
        updateCurrentFloorUseCase = UpdateCurrentFloorUseCase(floorRepository)
    }

    @Test
    fun shouldUpdateCurrentFloor() {
        floorRepository.getCurrentFloor().subscribe(testObserver)
        updateCurrentFloorUseCase(0.0)
        updateCurrentFloorUseCase(5.0)

        testObserver.assertNoErrors()
        testObserver.assertValueCount(2)
        testObserver.assertValues(Floor(0), Floor(1))
    }

}
