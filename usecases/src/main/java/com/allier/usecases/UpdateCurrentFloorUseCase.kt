package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Height
import javax.inject.Inject

/**
 * Updates current floor value
 */
class UpdateCurrentFloorUseCase @Inject constructor(private val floorRepository: FloorRepository) {

    /**
     * Updates current floor based on provided altitude
     */
    operator fun invoke(altitude: Double) = floorRepository.updateCurrentFloor(Height(altitude))

}
