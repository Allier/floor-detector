package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Floor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Provides current floor data
 */
class GetCurrentFloorUseCase @Inject constructor(private val floorRepository: FloorRepository) {

    /**
     * Provides infinite observable to a current floor data and notifies each time a floor has changed
     */
    operator fun invoke(): Observable<Floor> = floorRepository.getCurrentFloor()

    /**
     * Provides current floor as an integer
     */
    fun getCurrentLevelOnly(): Int = floorRepository.getCurrentFloorValue().level

}
