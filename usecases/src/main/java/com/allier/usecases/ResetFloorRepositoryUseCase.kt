package com.allier.usecases

import com.allier.data.FloorRepository
import javax.inject.Inject

/**
 * Resetting Floor repository to initial state
 */
class ResetFloorRepositoryUseCase @Inject constructor(private val floorRepository: FloorRepository) {

    operator fun invoke() = floorRepository.reset()

}
