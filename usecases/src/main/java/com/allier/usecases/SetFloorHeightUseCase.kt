package com.allier.usecases

import com.allier.data.FloorRepository
import com.allier.domain.Height
import javax.inject.Inject

/**
 * Setting floor height parameter
 */
class SetFloorHeightUseCase @Inject constructor(private val floorRepository: FloorRepository) {

    /**
     * Setting floor Height to corresponding Double value
     */
    operator fun invoke(value: Double) {
        floorRepository.floorHeight = Height(value)
    }

}
