package com.allier.usecases

import com.allier.data.FloorRepository
import javax.inject.Inject

/**
 * Provides current floor height setting
 */
class GetFloorHeightUseCase @Inject constructor(private val floorRepository: FloorRepository) {

    /**
     * Provides current floor height as Double value
     */
    operator fun invoke() = floorRepository.floorHeight.value

}
