package com.allier.data

import com.allier.domain.Floor
import com.allier.domain.Height
import com.allier.domain.div
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * Repository that provides current floor information
 * and holds floor height setting
 */
class FloorRepository {

    var floorHeight: Height = Height(4.0)

    private var lastFloor: Floor? = null
    private val floorSubject: BehaviorSubject<Floor> = BehaviorSubject.create()

    /**
     * Provides current floor value as an observable
     * which will notify each time the floor is changed
     */
    fun getCurrentFloor() = floorSubject as Observable<Floor>

    /**
     * Provides current floor information
     */
    fun getCurrentFloorValue(): Floor = floorSubject.value ?: Floor(0)

    /**
     * Updates current floor value based on height parameter (in meters)
     */
    fun updateCurrentFloor(height: Height) {
        val newFloor = Floor((height / floorHeight.value).toInt())
        if (newFloor != lastFloor) {
            floorSubject.onNext(newFloor)
            lastFloor = newFloor
        }
    }

    /**
     * Resets FloorRepository to its initial state by dropping last saved floor level
     */
    fun reset() {
        lastFloor = null
    }

}